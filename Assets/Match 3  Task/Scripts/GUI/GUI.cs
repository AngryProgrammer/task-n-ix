﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUI : MonoBehaviour {
	public static GUI instance;

	public GameObject gameOverPanel;
	public Text yourScoreTxt;
	public Text highScoreTxt;

	public Text scoreTxt;

	private int score, moveCounter;

	void Awake() {
		instance = GetComponent<GUI>();
		moveCounter = 15;
	}

	public void GameOver() {
		gameOverPanel.SetActive(true);

		if (score > PlayerPrefs.GetInt("HighScore")) {
			PlayerPrefs.SetInt("HighScore", score);
			highScoreTxt.text = "New Best: " + PlayerPrefs.GetInt("HighScore").ToString();
		} else {
			highScoreTxt.text = "Best: " + PlayerPrefs.GetInt("HighScore").ToString();
		}

		yourScoreTxt.text = score.ToString();
	}

	public int Score {
		get {
			return score;
		}

		set {
			score = value;
			scoreTxt.text = score.ToString();
		}
	}

	public int MoveCounter {
		get {
			return moveCounter;
		}

		set {
			moveCounter = value;
			if (moveCounter <= 0) {
				moveCounter = 0;
				StartCoroutine(WaitForShifting());
			}
		}
	}

	private IEnumerator WaitForShifting() {
		yield return new WaitUntil(() => !Board.instance.IsShifting);
		yield return new WaitForSeconds(.25f);
		GameOver();
	}

}
